import socket
import sys
import os

import quixote.server.scgi_server
import quixote.server.simple_server

import publisher

def start(args):
    run_function = quixote.server.scgi_server.run
    run_kwargs = {
        'port': 3010,
        'script_name': ''
    }

    i = 0
    while i < len(args):
        if args[i] == '--port':
            run_kwargs['port'] = int(args[i+1])
            i += 1
        elif args[i] == '--extra':
            publisher.LcsPublisher.register_extra_dir(args[i+1])
            i += 1
        elif args[i] == '--app-dir':
            publisher.LcsPublisher.APP_DIR = args[i+1]
            i += 1
        elif args[i] == '--data-dir':
            publisher.LcsPublisher.DATA_DIR = args[i+1]
            i += 1
        elif args[i] == '--http':
            run_function = quixote.server.simple_server.run
            del run_kwargs['script_name']
        elif args[i] == '--silent':
            sys.stdout = file('/dev/null', 'w')
            sys.stderr = file('/dev/null', 'w')
        i += 1

    try:
        run_function(publisher.LcsPublisher.create_publisher, **run_kwargs)
    except socket.error, e:
        if e[0] == 98:
            print >> sys.stderr, 'address already in use'
            sys.exit(1)
        raise
    except KeyboardInterrupt:
        sys.exit(1)

