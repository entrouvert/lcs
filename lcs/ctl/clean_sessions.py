import time
import sys
import os

import lcs
from lcs import sessions

def clean_vhost_sessions():
    manager = sessions.StorageSessionManager()
    one_week_ago = time.time() - 2*86400
    one_month_ago = time.time() - 30*86400
    for session_key in manager.keys():
        try:
            session = manager.get(session_key)
        except AttributeError:
            del manager[session_key]
            continue
        if session._access_time < one_week_ago or session._creation_time < one_month_ago:
            del manager[session.id]

def clean_sessions(args):
    publisher = lcs.create_publisher()

    if '--single_host' in args:
        clean_vhost_sessions()
    else:
        hostnames = os.listdir(lcs.APP_DIR)
        for hostname in hostnames:
            publisher.app_dir = os.path.join(lcs.APP_DIR, hostname)
            clean_vhost_sessions()

