import os

from Defaults import *

try:
    from lcs_cfg import *
except ImportError:
    pass

from qommon.publisher import set_publisher_class, QommonPublisher

from root import RootDirectory
import sessions

from users import User

class LcsPublisher(QommonPublisher):
    APP_NAME = 'lcs'
    APP_DIR = APP_DIR
    DATA_DIR = DATA_DIR
    ERROR_LOG = ERROR_LOG

    root_directory_class = RootDirectory
    session_manager_class = sessions.StorageSessionManager
    user_class = User

    def get_backoffice_module(cls):
        import backoffice
        return backoffice
    get_backoffice_module = classmethod(get_backoffice_module)

    def get_admin_module(cls):
        import admin
        return admin
    get_admin_module = classmethod(get_admin_module)


set_publisher_class(LcsPublisher)
LcsPublisher.register_extra_dir(os.path.join(os.path.dirname(__file__), 'extra'))

