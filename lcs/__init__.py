try:
    from quixote.ptl import compile_package
    compile_package(__path__)
except ImportError:
    pass
import sys
import os
sys.path.insert(0, os.path.dirname(__file__))

try:
    import qommon
except ImportError:
    print >> sys.stderr, 'Failed to import module \'qommon\''
    print >> sys.stderr, '  export CVSROOT=... (for w.c.s., see labs)'
    print >> sys.stderr, '  (cd .. && cvs checkout -d lasso-conformance-sp/lcs/qommon wcs/wcs/qommon)'
    sys.exit(1)

import lasso
if not hasattr(lasso, 'SAML2_SUPPORT'):
    lasso.SAML2_SUPPORT = False

