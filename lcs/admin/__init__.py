try:
    from quixote.ptl import compile_package
    compile_package(__path__)
except ImportError:
    pass
from root import RootDirectory, register_page

