import os

from quixote import get_publisher

from qommon.storage import StorableObject

class User(StorableObject):
    _names = 'users'

    name = None
    email = None
    is_admin = False
    anonymous = False

    name_identifiers = None
    identification_token = None
    lasso_dump = None

    def __init__(self, name = None):
        StorableObject.__init__(self)
        self.name = name
        self.name_identifiers = []
        self.roles = []

    def get_display_name(self):
        if self.anonymous:
            return _('Anonymous User')
        if self.name:
            return self.name
        if self.email:
            return self.email
        return _('Unknown User')
    display_name = property(get_display_name)

