#! /usr/bin/env python

import sys

def print_usage():
    print 'Usage: lcsctl.py command [...]'
    print ''
    print 'Commands:'
    print '  start                start server'
    print '  clean_sessions       clean old sessions'


if len(sys.argv) < 2:
    print_usage()
    sys.exit(1)
else:
    command = sys.argv[1]

    if command == 'start':
        from lcs.ctl.start import start
        start(sys.argv[2:])
    elif command == 'clean_sessions':
        from lcs.ctl.clean_sessions import clean_sessions
        clean_sessions(sys.argv[2:])
    else:
        print_usage()

