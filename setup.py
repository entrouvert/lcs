#! /usr/bin/env python

import os
import distutils.core
from quixote.ptl.qx_distutils import qx_build_py

local_cfg = None
if os.path.exists('lcs/lcs_cfg.py'):
    local_cfg = file('lcs/lcs_cfg.py').read()
    os.unlink('lcs/lcs_cfg.py')

def data_tree(destdir, sourcedir):
    extensions = ['.css', '.png', '.jpeg', '.jpg', '.xml', '.html', '.js']
    r = []
    for root, dirs, files in os.walk(sourcedir):
        l = [os.path.join(root, x) for x in files if os.path.splitext(x)[1] in extensions]
        r.append( (root.replace(sourcedir, destdir, 1), l) )
        if 'CVS' in dirs:
            dirs.remove('CVS')
    return r

distutils.core.setup(
        name = 'lcs',
        version = '0.0.0',
        maintainer = 'Frederic Peters',
        maintainer_email = 'fpeters@entrouvert.com',
        url = 'http://lasso.entrouvert.org',
        package_dir = { 'lcs': 'lcs' },
        packages = ['lcs', 'lcs.admin', 'lcs.backoffice', 'lcs.ctl', 'lcs.qommon',
                'lcs.qommon.admin', 'lcs.qommon.ident', 'lcs.qommon.backoffice'],
        cmdclass = {'build_py': qx_build_py},
        scripts = ['lcsctl.py'],
        data_files = data_tree('share/lcs/web/', 'root/')
    )

if local_cfg:
    file('lcs/lcs_cfg.py', 'w').write(local_cfg)
